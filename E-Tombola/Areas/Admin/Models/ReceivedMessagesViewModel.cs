﻿using E_Tombola.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Tombola.Areas.Admin.Models
{
    public class ReceivedMessagesViewModel
    {
        //
        public IList<Message> newMessages { get; set; }
        public IList<Message> receivedMessages { get; set; }
    }
}