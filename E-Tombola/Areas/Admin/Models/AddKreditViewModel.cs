﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_Tombola.Areas.Admin.Models
{
    public class AddKreditViewModel
    {
        [DisplayName("Počet kreditov")]
        [Required(ErrorMessage="Zadajte počet kreditov")]
        public int number { get; set; }

        [DisplayName("Cena")]
        [Required(ErrorMessage="Zadajte hodnotu pridávaných kreditov")]
        public double Price { get; set; }
        public string UserName { get; set; }
        public int profileId { get; set; }
    }
}