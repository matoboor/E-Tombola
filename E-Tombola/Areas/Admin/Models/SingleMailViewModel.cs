﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_Tombola.Areas.Admin.Models
{
    public class SingleMailViewModel
    {
        [Required(ErrorMessage = "Zadajte odosielatela")]
        [DisplayName("Odosielatel")]
        public string From { get; set; }

        [Required(ErrorMessage = "Zadajte príjemcu")]
        [DisplayName("Príjemca")]
        public string To { get; set; }

        [Required(ErrorMessage="Zadajte predmet správy")]
        [DisplayName("Predmet")]
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}