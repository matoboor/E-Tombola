﻿using E_Tombola.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Tombola.Areas.Admin.Models
{
    public class DashBoardViewModel
    {
        public IList<Raffle> ActiveRaffles { get; set; }

        public IList<Raffle> ClosestDrawingRaffles { get; set; }
        public IList<Purchase> UnconfirmedPurchases { get; set; }

        public IList<Message> UnreadedMessages { get; set; }

        public int NumberOfUsers { get; set; }
    }
}