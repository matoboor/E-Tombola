﻿using E_Tombola.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Tombola.Areas.Admin.Models
{
    public class ProfileViewModel
    {
        public Profile profile;
        public int purchasesNumber{
            get 
            {
                if(this.profile.Purchases!=null)
                {
                    return this.profile.Purchases.Count;
                }
                else
                {
                    return 0;
                }
            }
        }
        public int ticketNumbers;
    }
}