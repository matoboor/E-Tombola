﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_Tombola.Areas.Admin.Models
{
    public class DeleteUserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }

        public int A { get; set; }
        public int B { get; set; }

        [Required(ErrorMessage="Vypočítaj príklad")]
        public int Result { get; set; }

        public bool Correct
        {
            get
            {
                return (Result == A + B);
            }
        }
    }
}