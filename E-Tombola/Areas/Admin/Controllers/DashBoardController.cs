﻿using E_Tombola.Areas.Admin.Models;
using E_Tombola.Models;
using E_Tombola.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Tombola.Areas.Admin.Controllers
{
    [Authorize(Roles="Administrator")]
    public class DashBoardController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        //
        // GET: /Admin/DashBoard/
        public ActionResult Index()
        {
            DashBoardViewModel model = new DashBoardViewModel();
            model.ActiveRaffles = unitOfWork.RaffleRepository.Get().Where(r => r.IsActive).Take(5).ToList();
            model.ClosestDrawingRaffles = unitOfWork.RaffleRepository.Get().Where(r => (r.DateOfDrawing - DateTime.Now).TotalDays < 5).Take(5).ToList();
            var context = new ApplicationDbContext();
            model.NumberOfUsers = context.Users.Where(u => u.Roles.Count == 0).Count();
            model.UnconfirmedPurchases = unitOfWork.PurchaseRepository.Get().Where(p => p.Status != "OK").Take(5).ToList();
            model.UnreadedMessages = unitOfWork.MessageRepository.Get().Where(m => m.Type == "New").Take(5).ToList();
            return View(model);
        }
	}
}