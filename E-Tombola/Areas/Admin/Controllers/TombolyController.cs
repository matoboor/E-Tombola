﻿using E_Tombola.Models.Core;
using E_Tombola.Models.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Tombola.Areas.Admin.Controllers
{
    [Authorize(Roles="Administrator")]
    public class TombolyController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        //
        // GET: /Admin/Tomboly/
        public ActionResult Index()
        {
            IList<Raffle> raffles = unitOfWork.RaffleRepository.Get().ToList();
            return View(raffles);
        }

        //
        // GET: /Admin/Tomboly/Nova
        public ActionResult Nova()
        {
            Raffle newRaffle = new Raffle();
            newRaffle.StartOfTicketSale = DateTime.Now;
            newRaffle.EndOfTicketSale = DateTime.Now.AddDays(6);
            newRaffle.DateOfDrawing = DateTime.Now.AddDays(7);
            return View(newRaffle);
        }

        //
        // POST :/Admin/Tomboly/Nova
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Nova(Raffle newRaffle, HttpPostedFileBase file)
        {
            newRaffle.DateOfCreation = DateTime.Now;
            if(newRaffle.StartOfTicketSale>newRaffle.EndOfTicketSale || newRaffle.EndOfTicketSale>newRaffle.DateOfDrawing)
            {
                ModelState.AddModelError("", "Skontrolujte dátumy");
            }

            if (ModelState.IsValid)
                {
                if(file!=null && file.ContentLength>0)
                {
                    string url = "~/Uploads/" + newRaffle.Title + "-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(file.FileName);
                    string filePath = Server.MapPath(url);
                    file.SaveAs(filePath);
                    newRaffle.ImageURL = url;
                }
                else
                {
                    newRaffle.ImageURL = "~/Uploads/Default-raffle.jpg";
                }
                    unitOfWork.RaffleRepository.Insert(newRaffle);
                    unitOfWork.Save();
                    TempData["msg"] = "Tombola bola pridaná";
                    return RedirectToAction("Index");
                }
            else
            {
                ModelState.AddModelError("", "Nepodarilo sa uložiť záznam. Skontrolujte správnosť údajov.");
                return View(newRaffle);
            }


        }

        //
        // GET /Admin/Tomboly/Detail/1
        public ActionResult Detail(int Id)
        {
            Raffle raffleToView = unitOfWork.RaffleRepository.GetByID(Id);
            if (raffleToView==null)
            {
                return View("Error");
            }
            else
            {
                return View(raffleToView);
            }
        }

        //
        // GET /Admin/Tomboly/Edituj/1
        public ActionResult Edituj(int Id)
        {
            Raffle raffleToEdit = unitOfWork.RaffleRepository.GetByID(Id);
            if(raffleToEdit==null)
            {
                return View("Error");
            }
            else
            {
                return View(raffleToEdit);
            }
        }

        //
        // POST /Admin/Tomboly/Edituj/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edituj(Raffle RaffleToEdit, HttpPostedFileBase file)
        {
            TryUpdateModel(RaffleToEdit);
            if(ModelState.IsValid)
            {
                if(file!=null && file.ContentLength>0)
                {
                    if (!RaffleToEdit.ImageURL.Equals("~/Uploads/Default-raffle.jpg"))
                    {
                        System.IO.File.Delete(Server.MapPath(RaffleToEdit.ImageURL));
                    }
                    string url = "~/Uploads/" + RaffleToEdit.Title + "-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(file.FileName);
                    string path = Server.MapPath(url);
                    file.SaveAs(path);
                    RaffleToEdit.ImageURL = url;
                    

                }
                unitOfWork.RaffleRepository.Update(RaffleToEdit);
                unitOfWork.Save();
                TempData["msg"] = "Tombola bola upravená";
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", "Skontrolujte správanosť zadaných údajov");
                return View(RaffleToEdit);
            }
        }

        //
        // GET /Admin/Tomboly/Vymaz/1
        [HttpGet]
        public ActionResult Vymaz(string submitButton, int Id)
        {
            Raffle raffleToDelete = unitOfWork.RaffleRepository.GetByID(Id);
            if(raffleToDelete==null)
            {
                return RedirectToAction("Error");
            }
            else
            {
                if(raffleToDelete.Tickets.Count>0)
                {
                    ModelState.AddModelError("", "Tombola sa nedá vymazať. Sú už predané lístky.");
                }
                return View(raffleToDelete);
            }
        }

        //
        // POST /Admin/Tomboly/Vymaz/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Vymaz(int Id)
        {
            string imgUrl = unitOfWork.RaffleRepository.GetByID(Id).ImageURL;
            if(!imgUrl.Equals("~/Uploads/Default-raffle.jpg"))
            {
                System.IO.File.Delete(Server.MapPath(imgUrl));
            }
            unitOfWork.RaffleRepository.Delete(Id);
            unitOfWork.Save();
            TempData["msg"] = "Tombola bola vymazaná";
            return RedirectToAction("Index");
        }

        public ActionResult Zrebovanie(int? id)
        {
            if((bool)HttpContext.Application["Zrebovanie"])
            {
                id = (int)HttpContext.Application["ZrebovanieId"];
            }
            Raffle raffle = unitOfWork.RaffleRepository.GetByID(id);
            if(raffle==null)
            {
                return View("Error");
            }
            return View(raffle);
        }

        [HttpPost]
        public ActionResult Zrebovanie(Raffle model, string Command)
        {
            Raffle raffle = unitOfWork.RaffleRepository.GetByID(model.Id);
            switch (Command)
            {
                case "Start Stream":
                    HttpContext.Application["Zrebovanie"] = true;
                    HttpContext.Application["ZrebovanieId"] = model.Id;
                    break;
                case "Stop Stream":
                    HttpContext.Application["Zrebovanie"] = false;
                    HttpContext.Application["ZrebovanieId"] = null;
                    break;
                default:
                    break;
            }
            
            return View(raffle);
        }

        public ActionResult ELos(int id)
        {
            List<int> model = unitOfWork.TicketRepository.Get().Where(m => m.RaffleId==id).Select(m => m.Id).ToList();
            ViewBag.RaffleId = id;
            return View(model);
        }

        public ActionResult Vysledok(int raffleId, int winnerTicketId)
        {
            Raffle raf = unitOfWork.RaffleRepository.GetByID(raffleId);
            if (raf != null)
            {
                raf.WinnerTicketId = winnerTicketId;
                unitOfWork.RaffleRepository.Update(raf);
                unitOfWork.Save();
                TempData["msg"] = "Víťaz tomboly bol úspešne vylosovaný a zapísaný";
                // email
                return RedirectToAction("Zrebovanie");
            }
            return View("Error");
        }
	}
}