﻿using E_Tombola.Areas.Admin.Models;
using E_Tombola.Models;
using E_Tombola.Models.Core;
using E_Tombola.Models.Data;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace E_Tombola.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UzivateliaController : Controller
    {
        public UzivateliaController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public UzivateliaController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
            UserManager.UserValidator = new UserValidator<ApplicationUser>(UserManager) { AllowOnlyAlphanumericUserNames = false };
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        private UnitOfWork unitOfWork = new UnitOfWork();
        //
        // GET: /Admin/Uzivatelia/Administrator
        public ActionResult Index(string show)
        {
            var context = new ApplicationDbContext();
            if (show ==null || show.Equals("Zakaznici"))
            {
                ViewBag.Z = true;
                ViewBag.A = false;
                try
                {
                    List<ApplicationUser> users = context.Users.Where(u => u.Roles.Count == 0).ToList();
                    return View(users);
                }
                catch(Exception e)
                {
                    TempData["msg"] = e.InnerException.Message.ToString();
                    return View("Error");
                }
                //foreach(var usr in users)
                //{
                //    context.Users.Remove(usr);
                //}
                //foreach(var p in unitOfWork.ProfileRepository.Get())
                //{
                //    unitOfWork.ProfileRepository.Delete(p);
                //}
                //context.SaveChanges();
                //unitOfWork.Save();

            }
            else if (show.Equals("Administratori"))
            {
                try
                {
                    ViewBag.Z = false;
                    ViewBag.A = true;
                    List<ApplicationUser> users = context.Users.Where(u => u.Roles.Count > 0).ToList();
                    return View(users);
                }
                catch (Exception e)
                {
                    TempData["msg"] = e.InnerException.Message.ToString();
                    return View("Error");
                }
            }
            else return View("Error");
            
            
        }

        public ActionResult Novy()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Novy(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Administrator");
                    TempData["msg"] = "Administrátor pridaný";
                    return RedirectToAction("Index");
                }
                else
                {
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Admin/Uzivatelia/Profil/5
        public ActionResult Profil(string username)
        {
            Profile profil = unitOfWork.ProfileRepository.Get(includeProperties:"Purchases").FirstOrDefault(p => p.UserName == username);
            IList<Ticket> tickets = unitOfWork.TicketRepository.Get().Where(t => t.UserName == username).ToList();
            ProfileViewModel model = new ProfileViewModel();
            model.profile = profil;
            model.ticketNumbers = unitOfWork.TicketRepository.Get().Where(t => t.UserName == username).Count();
            if (model.profile != null)
            {
                return View(model);
            }
            else return View("Error");
        }

        //
        // GET: /Admin/Uzivatelia/MojProfil
        
        public ActionResult MojProfil()
        {
            return View();
        }

        //
        // POIS: /Admin/Uzivatelia/MojProfil

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> MojProfil(ManageUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    TempData["msg"] = "Heslo bolo zmenené";
                    return RedirectToAction("Index");
                }
                else
                {
                    AddErrors(result);
                }
            }
            return View(model);
        }


        //
        // GET: /Admin/Uzivatelia/Vymaz/{id}

        public ActionResult Vymaz(string Id)
        {
           try
           {
               var context = new ApplicationDbContext();
               ApplicationUser user = context.Users.FirstOrDefault(u => u.Id.Equals(Id));
               if(user==null)
               {
                   return View("Error");
               }
               Random rnd = new Random();
               DeleteUserViewModel model = new DeleteUserViewModel();
               model.Id = Id;
               model.UserName = user.UserName;
               model.A = rnd.Next(25);
               model.B = rnd.Next(25);
               return View(model);
           }
            catch(Exception e)
           {
               TempData["msg"] = e.InnerException.Message;
               return View("Error");
           }
        }

        //
        // POST /Admin/Uzivatelia/Vymaz/{Id}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Vymaz(DeleteUserViewModel model)
        {
            if(model.Correct)
            {
               var context = new ApplicationDbContext();
               ApplicationUser user = context.Users.Find(model.Id);
                if(user==null)
                {
                    return View("Error");
                }
                context.Users.Remove(user);
                context.SaveChanges();
                TempData["msg"] = "Administrátor bol odstránený";
                return RedirectToAction("Index", new { show = "Administratori" });
            }
            else
            {
                ModelState.AddModelError("Result", "Nevieš počítať???");
                return View(model);
            }
        }

        //
        //GET: /Admin/Uzivatelia/Kredit/2
        public ActionResult Kredit(int? id, string username)
        {
            Profile profile;
            if (id != null)
            {
                profile = unitOfWork.ProfileRepository.GetByID(id);
            }
            else
            {
                profile = unitOfWork.ProfileRepository.Get().FirstOrDefault(p => p.UserName == username);
            }
            if ( profile!= null)
            {
                AddKreditViewModel model = new AddKreditViewModel();
                model.profileId = profile.ProfileId;
                model.UserName = profile.UserName;
                return View(model);
            }
            else return View("Error");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Kredit(AddKreditViewModel model)
        {
            if(model.number>0 && model.Price>0)
            {
                Profile profile = unitOfWork.ProfileRepository.GetByID(model.profileId);
                profile.Credits =profile.Credits+ model.number;
                Purchase purchase = new Purchase();
                purchase.Amount = model.number;
                purchase.PaymentMethod = "Manual " + User.Identity.Name;
                purchase.Price = model.Price;
                purchase.Status = "OK";
                purchase.Time = DateTime.Now;
                purchase.UserName = profile.UserName;
                profile.Purchases.Add(purchase);
                unitOfWork.ProfileRepository.Update(profile);

                unitOfWork.Save();
                TempData["msg"] = "Kredit bol pridaný";
                return RedirectToAction("Profil", new { username = profile.UserName });
            }
            else
            {
                ModelState.AddModelError("", "Kredit musí byť kladné číslo");
                return View(model);
            }
        }

        //
        //GET: /Admin/Nakupy/2
        public ActionResult Nakupy(int id)
        {
            Profile profil = unitOfWork.ProfileRepository.Get(includeProperties: "Purchases").FirstOrDefault(p => p.ProfileId==id);
            if (profil != null)
            {
                return View(profil);
            }
            else return RedirectToAction("Error");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
	}


}