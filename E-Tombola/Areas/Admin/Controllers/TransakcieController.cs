﻿using E_Tombola.Models.Core;
using E_Tombola.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Tombola.Areas.Admin.Controllers
{
    public class TransakcieController : Controller
    {

        private UnitOfWork unitOfWork = new UnitOfWork();

        //
        // GET: /Admin/Transakcie/
        public ActionResult Index()
        {
            IList<Purchase> model = unitOfWork.PurchaseRepository.Get().ToList();
            return View(model);
        }

        //
        // GET: /Admin/Transakcie/Detail
        public ActionResult Detail(int Id)
        {
            Purchase model = unitOfWork.PurchaseRepository.GetByID(Id);
            if(model==null)
            {
                return View("Error");
            }
            return View(model);
        }
	}
}