﻿using E_Tombola.Areas.Admin.Models;
using E_Tombola.Areas.Admin.Models.MailService;
using E_Tombola.Models.Core;
using E_Tombola.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace E_Tombola.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class SpravyController : Controller
    {

        private Mailer mailService = new Mailer();
        private UnitOfWork unitOfWork = new UnitOfWork();

        //
        // GET: /Admin/Spravy/
        public ActionResult Index()
        {
            ViewBag.NewCount = unitOfWork.MessageRepository.Get().Where(m => m.Type == "new").Count();
            return View();
        }

        //
        // GET: /Admin/Spravy/Nova/adress
        public ActionResult Nova(string address)
        {
            SingleMailViewModel model = new SingleMailViewModel();
            if (address != null)
            {
                model.To = address;
            }
            model.From = "info@e-tombola.sk";
            List<string> adresses = unitOfWork.ProfileRepository.Get().Select(s => s.UserName).ToList();
            ViewBag.Adresses = adresses;
            return View(model);
        }

        //
        // POST: /Admin/Spravy/Nova/address
        [HttpPost]
        public ActionResult Nova(string address, SingleMailViewModel model)
        {
            if(ModelState.IsValid)
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(model.From);
                msg.To.Add(model.To);
                msg.Subject = model.Subject;
                msg.Body = model.Body;
                msg.IsBodyHtml = true;
                mailService.Send(msg);
                Message message = new Message();
                message.From = model.From;
                message.To = model.To;
                message.Subject = model.Subject;
                message.Body = model.Body;
                message.Type = "sent";
                message.Time = DateTime.Now;
                unitOfWork.MessageRepository.Insert(message);
                unitOfWork.Save();
                TempData["msg"] = "Správa bola úspešne odoslaná a uložená";
                return RedirectToAction("Index");
            }
            else
            {
                List<string> adresses = unitOfWork.ProfileRepository.Get().Select(s => s.UserName).ToList();
                ViewBag.Adresses = adresses;
                return View(model);
            }
        }

        //
        // GET /Admin/Spravy/Hromadna

        public ActionResult Hromadna()
        {
            SingleMailViewModel model = new SingleMailViewModel();
            model.From = "info@e-tombola.sk";
            int adresses = unitOfWork.ProfileRepository.Get().Select(s => s.UserName).Count();
            ViewBag.Receivers =  adresses;
            return View(model);
        }

        //
        // POST: /Admin/Spravy/Hromadna

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Hromadna(SingleMailViewModel model)
        {
            List<string> adresses = unitOfWork.ProfileRepository.Get().Select(s => s.UserName).ToList();
            if (ModelState.IsValid)
            {                
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(model.From);
                foreach (var a in adresses)
                {
                    msg.To.Add(a);
                }
                msg.Subject = model.Subject;
                msg.Body = model.Body;
                msg.IsBodyHtml = true;
                mailService.Send(msg);
                Message message = new Message();
                message.From = model.From;
                message.To = "All";
                message.Subject = model.Subject;
                message.Body = model.Body;
                message.Type = "sent";
                message.Time = DateTime.Now;
                unitOfWork.MessageRepository.Insert(message);
                unitOfWork.Save();
                TempData["msg"] = "Hromadná správa bola úspešne odoslaná a uložená";
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Adresses = adresses;
                return View(model);
            }
        }

        //
        // GET /Admin/Spravy/Odoslane
        public ActionResult Odoslane()
        {
            IList<Message> messages = unitOfWork.MessageRepository.Get().Where(m => m.Type == "sent").ToList();
            return View(messages);
        }


        public ActionResult Prijate()
        {
            ReceivedMessagesViewModel model = new ReceivedMessagesViewModel();
            model.newMessages = unitOfWork.MessageRepository.Get().Where(m => m.Type == "new").ToList();
            model.receivedMessages = unitOfWork.MessageRepository.Get().Where(m => m.Type == "viewed").ToList();
            return View(model);
        }

        //
        // GET /Admin/Spravy/Detail/5

        public ActionResult Detail(int id)
        {
            Message msg = unitOfWork.MessageRepository.GetByID(id);
            if (msg != null)
            {
                if (msg.Type == "new")
                {
                    msg.Type = "viewed";
                    unitOfWork.MessageRepository.Update(msg);
                    unitOfWork.Save();
                    TempData["msg"] = "Správa prečítaná";
                }
                return View(msg);
            }
            else return RedirectToAction("Error");
        }
	}
}