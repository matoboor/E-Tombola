namespace E_Tombola.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MessageId = c.Int(nullable: false, identity: true),
                        From = c.String(),
                        To = c.String(),
                        Subject = c.String(),
                        Body = c.String(),
                        Time = c.DateTime(nullable: false),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.MessageId);
            
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        ProfileId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Credits = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProfileId);
            
            CreateTable(
                "dbo.Purchases",
                c => new
                    {
                        PurchaseId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Amount = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        PaymentMethod = c.String(),
                        Status = c.String(),
                        Time = c.DateTime(nullable: false),
                        Profile_ProfileId = c.Int(),
                    })
                .PrimaryKey(t => t.PurchaseId)
                .ForeignKey("dbo.Profiles", t => t.Profile_ProfileId)
                .Index(t => t.Profile_ProfileId);
            
            CreateTable(
                "dbo.Raffles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 250),
                        Description = c.String(nullable: false, maxLength: 500),
                        ImageURL = c.String(),
                        Price = c.Double(nullable: false),
                        PriceOfTicket = c.Int(nullable: false),
                        DateOfCreation = c.DateTime(nullable: false),
                        DateOfDrawing = c.DateTime(nullable: false),
                        StartOfTicketSale = c.DateTime(nullable: false),
                        EndOfTicketSale = c.DateTime(nullable: false),
                        WinnerTicketId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RaffleId = c.Int(nullable: false),
                        UserName = c.String(),
                        PurchaseDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Raffles", t => t.RaffleId, cascadeDelete: true)
                .Index(t => t.RaffleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "RaffleId", "dbo.Raffles");
            DropForeignKey("dbo.Purchases", "Profile_ProfileId", "dbo.Profiles");
            DropIndex("dbo.Tickets", new[] { "RaffleId" });
            DropIndex("dbo.Purchases", new[] { "Profile_ProfileId" });
            DropTable("dbo.Tickets");
            DropTable("dbo.Raffles");
            DropTable("dbo.Purchases");
            DropTable("dbo.Profiles");
            DropTable("dbo.Messages");
        }
    }
}
