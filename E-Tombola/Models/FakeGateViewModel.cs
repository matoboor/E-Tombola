﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Tombola.Models
{
    public class FakeGateViewModel
    {
        public int Id { get; set; }
        public int Price { get; set; }

        public string ReturnUrl { get; set; }
    }
}