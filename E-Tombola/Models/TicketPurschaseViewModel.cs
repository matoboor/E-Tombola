﻿using E_Tombola.Models.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace E_Tombola.Controllers
{
    public class TicketPurschaseViewModel
    {
        public Raffle raffle { get; set; }

        [Required(ErrorMessage="Zadajte množstvo tiketov ktoré chcete kúpiť")]
        [Range(1, 1000, ErrorMessage = "Počet musí byť medzi 1 a 1000")]
        [DisplayName("Počet lístkov")]
        public int Count { get; set; }

       public TicketPurschaseViewModel()
        {
            Count = 1;
        }
    }
}
