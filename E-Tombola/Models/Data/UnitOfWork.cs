﻿using E_Tombola.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Tombola.Models.Data
{
    public class UnitOfWork
    {
        private TombolaContext context = new TombolaContext();
        private GenericRepository<Ticket> ticketRepository;
        private GenericRepository<Raffle> raffleRepository;
        private GenericRepository<Profile> profileRepository;
        private GenericRepository<Purchase> purchaseRepository;
        private GenericRepository<Message> messageRepository;

        public GenericRepository<Ticket> TicketRepository
        {
            get
            {
                if(this.ticketRepository ==null)
                {
                    this.ticketRepository = new GenericRepository<Ticket>(context);
                }
                return ticketRepository;
            }
        }

        public GenericRepository<Raffle> RaffleRepository
        {
            get
            {
                if(this.raffleRepository==null)
                {
                    this.raffleRepository = new GenericRepository<Raffle>(context);
                }
                return raffleRepository;
            }
        }

        public GenericRepository<Profile> ProfileRepository
        {
            get
            {
                if(this.profileRepository==null)
                {
                    this.profileRepository = new GenericRepository<Profile>(context);
                }
                return profileRepository;
            }
        }

        public GenericRepository<Purchase> PurchaseRepository
        {
            get
            {
                if(this.purchaseRepository==null)
                {
                    this.purchaseRepository = new GenericRepository<Purchase>(context);
                }
                return purchaseRepository;
            }
        }

        public GenericRepository<Message> MessageRepository
        {
            get
            {
                if(this.messageRepository==null)
                {
                    this.messageRepository = new GenericRepository<Message>(context);
                }
                return messageRepository;
            }
        }
        public void Save()
        {
            context.SaveChanges();
        }
    }
}