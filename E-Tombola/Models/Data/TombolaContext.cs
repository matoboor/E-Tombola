﻿using E_Tombola.Models.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace E_Tombola.Models.Data
{
    public class TombolaContext : DbContext
    {

        public TombolaContext()
          :base("DefaultConnection")
        {
        }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<Raffle> Raffles { get; set; }

        public DbSet<Purchase> Purchases { get; set; }

        public DbSet<Profile> Profiles { get; set; }

        public DbSet<Message> Messages { get; set; }

    }
}