﻿using E_Tombola.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Tombola.Models
{
    public class IndexViewModel
    {
        public Raffle Raffle { get; set; }
    }
}