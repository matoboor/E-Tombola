﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_Tombola.Models
{
    public class KreditAmount
    {
        public int Number { get; set; }
        public string Description { get; set; }
    }
    public class PurchaseKreditViewModel
    {
        [Required(ErrorMessage="Vyberte si prosím množstvo")]
        public int SelectedAmount { get; set; }
        public List<KreditAmount> listOfAmounts = new List<KreditAmount>();

    }
}