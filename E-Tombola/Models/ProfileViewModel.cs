﻿using E_Tombola.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Tombola.Models
{
    public class UserProfileViewModel
    {
        public Profile Profile { get; set; }
        public IList<Ticket> ActiveTickets { get; set; }

        public IList<Ticket> OldTickets { get; set; }

        public IList<Ticket> WinTickets { get; set; }


    }
}