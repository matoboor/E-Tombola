﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace E_Tombola.Models.Core
{
    public class Profile
    {
        public int ProfileId { get; set; }
        [DisplayName("Uživateľské meno")]
        public string UserName { get; set; }

        [DisplayName("Počet kreditov")]
        public int Credits { get; set; }

        [DisplayName("História nákupov")]
        public IList<Purchase> Purchases { get; set; }

        public Profile()
        {
            Purchases = new List<Purchase>();
        }
    }
}