﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace E_Tombola.Models.Core
{
    public class Purchase
    {
        [DisplayName("ID Transakcie")]
        public int PurchaseId { get; set; }

        [DisplayName("Meno zákazníka")]
        public string UserName { get; set; }

        [DisplayName("Počet kreditov")]
        public int Amount { get; set; }

        [DisplayName("Cena")]
        public double Price { get; set; }

        [DisplayName("Platobná metóda")]
        public string PaymentMethod { get; set; }

        [DisplayName("Status")]
        public string Status { get; set; }

        [DisplayName("Čas")]
        public DateTime Time { get; set; }

    }


}
