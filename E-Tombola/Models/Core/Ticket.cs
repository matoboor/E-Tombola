﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_Tombola.Models.Core
{
    public class Ticket
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        public int RaffleId { get; set; }
        public string UserName { get; set; }
        
        [DisplayName("Dátum nákupu")]
        public DateTime PurchaseDate { get; set; }

        public virtual Raffle Raffle { get; set; }

        public bool IsWin
        {
            get
            {
                if(this.Raffle!=null && this.Raffle.WinnerTicketId!=null)
                {
                    return RaffleId == Raffle.WinnerTicketId;
                }
                return false;
            }
        }
        public Ticket()
        {
            PurchaseDate = DateTime.Now;
        }
    }
}