﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_Tombola.Models.Core
{
    public class Raffle
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }


        [DisplayName("Názov")]
        [Required(ErrorMessage="Názov je povinný atribút")]
        [StringLength(250)]
        public string Title { get; set; }

        [DisplayName("Popis")]
        [Required(ErrorMessage="Napíšte aspoň krátky popis tomboly")]
        [StringLength(500)]
        public string Description { get; set; }

        [DisplayName("Obrázok")]
        public string ImageURL { get; set; }

        [DisplayName("Hodnota ceny")]
        [Required(ErrorMessage="Zadajte približnú hodnotu ceny")]
        public double Price { get; set; }

        [DisplayName("Cena lístka")]
        [Required(ErrorMessage="Zadajte cenu jedného lístka")]
        [Range(1,10,ErrorMessage="Zadajte celé číslo od {0} do {1}")]
        public int PriceOfTicket { get; set; }

        [DisplayName("Dátum vytvorenia")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString="{0:f}", ApplyFormatInEditMode = true )]
        public DateTime DateOfCreation { get; set; }

        [DisplayName("Dátum losovania")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:f}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage="Zadajte správny dátum")]
        public DateTime DateOfDrawing { get; set; }

        [DisplayName("Začiatok predaja lístkov")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:f}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Zadajte správny dátum")]
        public DateTime StartOfTicketSale { get; set; }

        [DisplayName("Koniec predaja lístkov")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:f}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Zadajte správny dátum")]
        public DateTime EndOfTicketSale { get; set; }

        [DisplayName("Je aktívna?")]
        public bool IsActive
        {
            get
            {
                return ((DateTime.Now<=DateOfDrawing)&&(DateTime.Now>=StartOfTicketSale));
            }
        }

        [DisplayName("Vylosovaný lístok")]
        public int? WinnerTicketId { get; set; }

        public virtual IList<Ticket> Tickets { get; set; }

        [DisplayName("Počet predaných lístkov")]
        public int NumberOfTickets
        {
            get
            {
                if (Tickets==null)
                {
                    return 0;
                }
                else
                {
                    return Tickets.Count;
                }
            }
        }
        public Raffle()
        {
            Tickets = new List<Ticket>();
            PriceOfTicket = 1;
        }
    }
}