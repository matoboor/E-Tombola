﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Tombola.Models.Core
{
    public class Message
    {
        public int MessageId { get; set; }

        [DisplayName("Odosielateľ")]
        public string From { get; set; }

        [DisplayName("Príjemca")]
        public string To { get; set; }

        [DisplayName("Predmet")]
        public string Subject { get; set; }

        [DisplayName("Telo správy")]
        [AllowHtml]
        public string Body { get; set; }

        [DisplayName("Čas")]
        public DateTime Time { get; set; }

        public string Type { get; set; }
    }
}