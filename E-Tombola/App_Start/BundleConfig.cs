﻿using System.Web;
using System.Web.Optimization;

namespace E_Tombola
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css"));

           bundles.Add(new StyleBundle("~/Content/frontcss").Include(
                      "~/Content/site.css"));
           bundles.Add(new StyleBundle("~/Content/backcss").Include(
                     "~/Content/adminsite.css"));

            bundles.Add(new StyleBundle("~/Content/datetimepicker").Include(
                      "~/Content/jquery.datetimepicker.css"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
                      "~/Scripts/jquery.datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/toast").Include(
                      "~/Scripts/jquery.toast.min.js"));
            bundles.Add(new StyleBundle("~/Content/toast").Include(
                      "~/Content/jquery.toast.css"));

            bundles.Add(new ScriptBundle("~/bundles/autocomplete").Include(
                      "~/Scripts/jquery.autocomplete.js"));
            bundles.Add(new StyleBundle("~/Content/autocomplete").Include(
                      "~/Content/jquery.autocomplete.css"));
        }
    }
}
