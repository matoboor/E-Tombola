﻿using E_Tombola.Models.Core;
using E_Tombola.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Tombola.Controllers
{
    public class TombolaController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        //
        // GET: /Tomboly/
        public ActionResult Index()
        {          
                List<Raffle> model = unitOfWork.RaffleRepository.Get().Where(r => r.IsActive).OrderBy(r => r.DateOfDrawing).ToList();
                return View(model);          
        }


        //
        // GET: /Tomboly/KupListky/5
        [Authorize]
        public ActionResult KupListky(int id)
        {
            if (!User.IsInRole("Administrator"))
            {
                TicketPurschaseViewModel model = new TicketPurschaseViewModel();
                model.raffle = unitOfWork.RaffleRepository.GetByID(id);
                if (model.raffle != null)
                {
                    return View(model);
                }
                else
                {
                    TempData["msg"] = "Tombola s ID: "+id+" neexistuje";
                    return View("Error");
                }
            }
            TempData["msg"] = "Táto funkcia nieje prístupná pre administrátora";
            return View("Error");
        }

        //
        // POST: /Tomboly/KupListky/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult KupListky(TicketPurschaseViewModel model)
        {
            if (ModelState.IsValid)
            {
                Raffle raffle = unitOfWork.RaffleRepository.GetByID(model.raffle.Id);
                if (raffle != null && raffle.EndOfTicketSale>DateTime.Now)
                {
                    Profile profile = unitOfWork.ProfileRepository.Get().Single(p => p.UserName.Equals(User.Identity.Name));
                    if (profile != null)
                    {
                        if (profile.Credits >= (raffle.PriceOfTicket * model.Count))
                        {
                            profile.Credits = profile.Credits - raffle.PriceOfTicket * model.Count;
                            for (int i = 0; i < model.Count; i++)
                            {
                                unitOfWork.TicketRepository.Insert(new Ticket() { PurchaseDate = DateTime.Now, RaffleId = raffle.Id, UserName = User.Identity.Name });
                            }
                            unitOfWork.Save();
                            TempData["msg"] = model.Count + " lístkov bolo zakúpených";
                            return RedirectToAction("Index", "Profil");
                        }
                        TempData["msg"] = "Nedostatok kreditov! Zakúpte kredit.";
                        return RedirectToAction("Index", "Kredit");
                    }
                    return View("Error");
                }
                return View("Error");
            }
            return View(model);
        }

        public ActionResult Archiv()
        {
            List<Raffle> model = unitOfWork.RaffleRepository.Get().Where(m => !m.IsActive).OrderBy(m => m.DateOfDrawing).ToList();
            return View(model);
        }

        //
        // ZREBOVANIE

        public ActionResult Zrebovanie()
        {
            if((bool)HttpContext.Application["Zrebovanie"])
            {
                Raffle raffle = unitOfWork.RaffleRepository.GetByID((int)HttpContext.Application["ZrebovanieId"]);
                ViewBag.Message = "true";
                return View(raffle);
            }
            else
            {
                ViewBag.Message = "false";
                return View();
            }
            
        }
	}
}