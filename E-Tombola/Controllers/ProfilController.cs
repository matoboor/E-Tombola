﻿using E_Tombola.Models;
using E_Tombola.Models.Core;
using E_Tombola.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Tombola.Controllers
{
    [Authorize]
    public class ProfilController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        //
        // GET: /Profil/
        public ActionResult Index()
        {
            if (!User.IsInRole("Administrator"))
            {
                UserProfileViewModel model = new UserProfileViewModel();
                model.ActiveTickets = unitOfWork.TicketRepository.Get().Where(t => t.Raffle.IsActive && t.UserName.Equals(User.Identity.Name)).OrderBy(t => t.RaffleId).ToList();
                model.OldTickets = unitOfWork.TicketRepository.Get().Where(t => !t.Raffle.IsActive && t.UserName.Equals(User.Identity.Name)).OrderBy(t => t.RaffleId).ToList();
                model.WinTickets = model.OldTickets.Where(t => t.IsWin).ToList();
                model.Profile = unitOfWork.ProfileRepository.Get().FirstOrDefault(p => p.UserName.Equals(User.Identity.Name));
                return View(model);
            }
            TempData["msg"] = "Adminístrator nemá prístup k tejto funkcií";
            return View("Error");
        }

        public ActionResult Nakupy()
        {
            if (User.IsInRole("Administrator"))
            {
                TempData["msg"] = "Táto sekcia nieje prístupná pre administrátora";
                return View("Error");
            }
            List<Purchase> model = unitOfWork.PurchaseRepository.Get().Where(m => m.UserName.Equals(User.Identity.Name)).OrderBy(m => m.Time).ToList();
            return View(model);
        }

        public ActionResult MiniProfil()
        {
            ViewBag.Credit = unitOfWork.ProfileRepository.Get().Where(m => m.UserName.Equals(User.Identity.Name)).Single().Credits;
            return PartialView();
        }
	}
}