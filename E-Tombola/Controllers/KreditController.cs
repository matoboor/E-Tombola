﻿using E_Tombola.Models;
using E_Tombola.Models.Core;
using E_Tombola.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Tombola.Controllers
{
    [Authorize]
    public class KreditController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        //
        // GET: /Kredit/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SMS()
        {
            if(User.IsInRole("Administrator"))
            {
                TempData["msg"] = "Táto sekcia nieje prístupná pre administrátora";
                return View("Error");
            }
            PurchaseKreditViewModel model = new PurchaseKreditViewModel();
            model.listOfAmounts.Add(new KreditAmount() { Number = 1, Description = "1 kredit (1,00 €) " });
            model.listOfAmounts.Add(new KreditAmount() { Number = 2, Description = "2 kredity (2,00 €)" });
            model.listOfAmounts.Add(new KreditAmount() { Number = 5, Description = "5 kreditov (5,00 €)" });
            model.listOfAmounts.Add(new KreditAmount() { Number = 10, Description = "10 kreditov (9,00 €)" });
            model.SelectedAmount = 1;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SMS(PurchaseKreditViewModel model)
        {
            if(ModelState.IsValid)
            {
                //presmerovanie na branu
                int amount = model.SelectedAmount;
                int price;
                if (amount == 10) {price = 9;}
                else { price = amount; };
                Purchase transaction = new Purchase();
                transaction.Amount = amount;
                transaction.Price = price;
                transaction.PaymentMethod = "SMS";
                transaction.Status = "unconfirmed";
                transaction.Time = DateTime.Now;
                transaction.UserName = User.Identity.Name;
                unitOfWork.PurchaseRepository.Insert(transaction);
                unitOfWork.Save();
                return RedirectToAction("FakeGate", new { id=transaction.PurchaseId, price=transaction.Price, returnURL=Url.Action("SMSPotvrdenie", "Kredit")});
            }
            else
            {
                return View(model);
            }
        }


        public ActionResult SMSPotvrdenie(int id, string status)
        {
            Purchase transaction = unitOfWork.PurchaseRepository.GetByID(id);
            if(transaction==null)
            {
                return View("Error");
            }
            if(transaction.Status.Equals("OK"))
            {
                return RedirectToAction("Index", "Profil");
            }
            if(status.Equals("OK"))
            {
                transaction.Status = "OK";
                Profile profile = unitOfWork.ProfileRepository.Get().FirstOrDefault(p => p.UserName.Equals(transaction.UserName));
                profile.Credits = profile.Credits + transaction.Amount;
                unitOfWork.PurchaseRepository.Update(transaction);
                unitOfWork.ProfileRepository.Update(profile);
                unitOfWork.Save();
                ViewBag.Message = "Gratulujeme. K Vašemu účtu bolo pripísaných " + transaction.Amount + " kreditov";
                TempData["msg"] = "Kredit bol kúpený";
                return View(transaction);
            }
            
            if (status.Equals("Canceled"))
            {
                transaction.Status = "Canceled";
                unitOfWork.PurchaseRepository.Update(transaction);
                unitOfWork.Save();
                ViewBag.Message = "Transakcia bola zrušená ";
                TempData["msg"] = "Transakcia zrušená!";
                return View(transaction);
            } 
            
            if (status.Equals("Error"))
            {
                transaction.Status = "Canceled";
                unitOfWork.PurchaseRepository.Update(transaction);
                unitOfWork.Save();
                ViewBag.Message = "Transakcia bola zrušená kvôli neznámej chybe na strane platobnej brány";
                TempData["msg"] = "Transakcia zrušená!";
                return View(transaction);
            }
            TempData["msg"] = "Obráťte sa na podporu";
            return View("Error");
        }


        public ActionResult Karta()
        {
            if (User.IsInRole("Administrator"))
            {
                TempData["msg"] = "Táto sekcia nieje prístupná pre administrátora";
                return View("Error");
            }
            PurchaseKreditViewModel model = new PurchaseKreditViewModel();
            model.listOfAmounts.Add(new KreditAmount() { Number = 5, Description = "5 kreditov (5,00 €) " });
            model.listOfAmounts.Add(new KreditAmount() { Number = 10, Description = "10 kreditov (10,00 €)" });
            model.listOfAmounts.Add(new KreditAmount() { Number = 15, Description = "15 kreditov (14,00 €)" });
            model.listOfAmounts.Add(new KreditAmount() { Number = 20, Description = "20 kreditov (18,00 €)" });
            model.SelectedAmount = 1;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Karta(PurchaseKreditViewModel model)
        {
            if (ModelState.IsValid)
            {
                //presmerovanie na branu
                int amount = model.SelectedAmount;
                int price;
                if (amount == 15) { price = 14; }
                else if (amount == 20) { price = 18; }
                else { price = amount; };
                Purchase transaction = new Purchase();
                transaction.Amount = amount;
                transaction.Price = price;
                transaction.PaymentMethod = "CreditCard";
                transaction.Status = "unconfirmed";
                transaction.Time = DateTime.Now;
                transaction.UserName = User.Identity.Name;
                unitOfWork.PurchaseRepository.Insert(transaction);
                unitOfWork.Save();
                return RedirectToAction("FakeGate", new { id = transaction.PurchaseId, price = transaction.Price, returnURL = Url.Action("KartaPotvrdenie", "Kredit") });
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult KartaPotvrdenie(int id, string status)
        {
            Purchase transaction = unitOfWork.PurchaseRepository.GetByID(id);
            if (transaction == null)
            {
                return View("Error");
            }
            if (transaction.Status.Equals("OK"))
            {
                return RedirectToAction("Index", "Profil");
            }
            if (status.Equals("OK"))
            {
                transaction.Status = "OK";
                Profile profile = unitOfWork.ProfileRepository.Get().FirstOrDefault(p => p.UserName.Equals(transaction.UserName));
                profile.Credits = profile.Credits + transaction.Amount;
                unitOfWork.PurchaseRepository.Update(transaction);
                unitOfWork.ProfileRepository.Update(profile);
                unitOfWork.Save();
                ViewBag.Message = "Gratulujeme. K Vašemu účtu bolo pripísaných " + transaction.Amount + " kreditov";
                TempData["msg"] = "Kredit bol kúpený";
                return View(transaction);
            }

            if (status.Equals("Canceled"))
            {
                transaction.Status = "Canceled";
                unitOfWork.PurchaseRepository.Update(transaction);
                unitOfWork.Save();
                ViewBag.Message = "Transakcia bola zrušená ";
                TempData["msg"] = "Transakcia zrušená!";
                return View(transaction);
            }

            if (status.Equals("Error"))
            {
                transaction.Status = "Canceled";
                unitOfWork.PurchaseRepository.Update(transaction);
                unitOfWork.Save();
                ViewBag.Message = "Transakcia bola zrušená kvôli neznámej chybe na strane platobnej brány";
                TempData["msg"] = "Transakcia zrušená!";
                return View(transaction);
            }
            TempData["msg"] = "Obráťte sa na podporu";
            return View("Error");
        }

        public ActionResult Prevod()
        {
            if (User.IsInRole("Administrator"))
            {
                TempData["msg"] = "Táto sekcia nieje prístupná pre administrátora";
                return View("Error");
            }
            return View();
        }
        public ActionResult FakeGate(int id, int price, string returnURL)
        {
            FakeGateViewModel model = new FakeGateViewModel();
            model.Id= id;
            model.Price = price;
            model.ReturnUrl = returnURL;
            return View(model);
        }

        [HttpPost]
        public ActionResult FakeGate(FakeGateViewModel model, string Command)
        {
            if (Command.Equals("Pay"))
            {
                return Redirect(model.ReturnUrl + "?id=" + model.Id + "&status=OK");
            }
            if (Command.Equals("Cancel"))
            {
                return Redirect(model.ReturnUrl + "?id=" + model.Id + "&status=Canceled");
            }
            return Redirect(model.ReturnUrl + "?id=" + model.Id + "&status=Error");
        }
	}
}