﻿using E_Tombola.Models;
using E_Tombola.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Tombola.Controllers
{
    public class HomeController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        public ActionResult Index()
        {
            IndexViewModel model = new IndexViewModel();
            model.Raffle = unitOfWork.RaffleRepository.Get().Where(m => m.IsActive).OrderBy(m => m.DateOfDrawing).FirstOrDefault();
            return View(model);
        }       
    }
}