﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(E_Tombola.Startup))]
namespace E_Tombola
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
